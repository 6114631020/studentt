import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;


public class Main {
	public static void main(String[] args) {
		Map<String,String>subject = new HashMap<String,String>();
		subject.put("1111", "English");
		subject.put("2222", "Thai Language");
		subject.put("3333", "Science");
		Student setSubjects = new Student();
		setSubjects.setSubject(subject);
		
		Student student1 = new Student();
		Student student2 = new Student();
		
		List<String> studentAdress = new ArrayList<String>();
		Set<String> tel = new HashSet<String>();
		
		studentAdress.add("201/2 Rayong");		
		tel.add("081-2345678");
		
		student1.setName("prayut");
		student1.setLastName("chanocha");
		student1.setSex(Sex.MALE);
		student1.setAddress(studentAdress);
		student1.setTel(tel);
		
		Map<String,Grade> grades = new HashMap<String,Grade>();
		
		Grade gradeEng1 = new Grade();
		gradeEng1.setGrade(4.0);
		gradeEng1.setCredit(3.0);
		gradeEng1.setSubjectID("1111"); // SubjectID "1111" = Subject English
		grades.put(gradeEng1.getSubjectID(),gradeEng1);
		
		Grade gradeThai1 = new Grade();
		gradeThai1 = new Grade();
		gradeThai1.setGrade(3.0);
		gradeThai1.setCredit(3.0);
		gradeThai1.setSubjectID("2222"); // SubjectID "2222" = Subject Thai Language
		grades.put(gradeThai1.getSubjectID(),gradeThai1);
		
		Grade gradeScirne1 = new Grade();
		gradeScirne1 = new Grade();
		gradeScirne1.setGrade(1.0);
		gradeScirne1.setCredit(3.0);
		gradeScirne1.setSubjectID("3333"); // SubjectID "1111" = Subject Science
		grades.put(gradeScirne1.getSubjectID(),gradeScirne1);
		
		student1.setGradeMap(grades);
		
		student1.printInfomationStudent();
		student1.printAddress();
		student1.printTel();
		student1.calculateGrade();

		studentAdress.clear();
		studentAdress.add("201/5 Rayong");
		tel.add("081-2345678");
		
		student2.setName("Parina");
		student2.setLastName("Kraigupt");
		student2.setSex(Sex.FEMALE);
		student2.setAddress(studentAdress);
		student2.setTel(tel);
		
		Grade gradeEng2 = new Grade();
		gradeEng2.setGrade(1.0);
		gradeEng2.setCredit(3.0);
		gradeEng2.setSubjectID("1111");
		grades.put(gradeEng2.getSubjectID(),gradeEng2);
		
		Grade gradeThai2 = new Grade();
		gradeThai2.setGrade(1.0);
		gradeThai2.setCredit(3.0);
		gradeThai2.setSubjectID("2222");
		grades.put(gradeThai2.getSubjectID(),gradeThai2);
		
		Grade gradeScirne2 = new Grade();
		gradeScirne2.setGrade(1.0);
		gradeScirne2.setCredit(3.0);
		gradeScirne2.setSubjectID("3333");
		grades.put(gradeScirne2.getSubjectID(),gradeScirne2);
		student2.setGradeMap(grades);
		
		System.out.println("");
		System.out.println("");
		student2.printInfomationStudent();
		student2.printAddress();
		student2.printTel();
		student2.calculateGrade();
		
	}

}
