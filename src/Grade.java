import java.util.Map;

public class Grade {
	private String subjectID;
	private Double grade; 		//Double grade เพื่อให้มีความสามารถในการเป็น null (ในกรณีที่ยังไม่ คำนวณ grade)
	private double credit;
	
	
	public double getCredit() {
		return credit;
	}
	public void setCredit(double credit) {
		this.credit = credit;
	}
	public String getSubjectID() {
		return subjectID;
	}
	public void setSubjectID(String subjectID) {
		this.subjectID = subjectID;
	}
	public double getGrade() {
		return grade;
	}
	public void setGrade(double grade) {
		this.grade = grade;
	}

	
	
	
}
