import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.security.auth.Subject;


public class Student {
	private static int studentIDRunning = 120211;
	private String studentID;
	private String name;
	private String lastName;
	private Sex sex;
	
	private static Map<String,String>subject;
	private Map<String,Grade> gradeMap;
	private List<String>address;
	private Set<String>tel;
	
	public Student() {
		this.studentID = "" + studentIDRunning ++;
	}

	public String getStudentID() {
		return studentID;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name.toLowerCase();
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName.toLowerCase();
	}

	public Sex getSex() {
		return sex;
	}

	public void setSex(Sex sex) {
		this.sex = sex;
	}

	public Map<String, Grade> getGradeMap() {
		return gradeMap;
	}

	public void setGradeMap(Map<String, Grade> gradeMap) {
		this.gradeMap = gradeMap;
	}
	
	public void printAddress() {
		System.out.println(address);
	}

	public void setAddress(List<String> address) {
		this.address = address;
	}

	public void printTel() {
		System.out.println("Tel" + tel);
	}

	public void setTel(Set<String> tel) {
		this.tel = tel;
	}

	public Map<String, String> getSubject() {
		return subject;
	}

	public void setSubject(Map<String, String> subject) {
		this.subject = subject;
	}

	public void calculateGrade() {
		double score = 0;
		double sumScore = 0;
		double credit = 0;
		for(Grade gradeMaps:gradeMap.values()) {
			System.out.println("Subject " + subject.get(gradeMaps.getSubjectID()) + " Credit " +
					gradeMaps.getCredit() + " Grade " + gradeMaps.getGrade());
			
			score = gradeMaps.getCredit() * gradeMaps.getGrade();
			sumScore = sumScore + score;
			credit = credit + gradeMaps.getCredit();
		}
		
		System.out.printf("Average grade " + "%.2f",sumScore/credit);
	}
	public void printInfomationStudent() {
		System.out.println("Student " + studentID);
		System.out.println("Name " + this.name + " " + this.lastName);
		System.out.println("Student ID " + this.studentID);
		
	}
}
//for(Map.Entry<String,Grade> gradeMaps:gradeMap.entrySet()) {
//	score = gradeMaps.getValue().getCredit() * gradeMaps.getValue().getGrade();
//	sumScore = sumScore + score;
//	credit = credit + gradeMaps.getValue().getCredit();
//}
